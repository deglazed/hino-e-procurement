<?php  
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Layout
{

    var $ci;
    var $layout;

    function __construct()
    {
        $this->CI =& get_instance();
        $this->layout = 'layout_main';
    }

    function setLayout($layout)
    {
      $this->layout = $layout;
    }
    

    function view($view, $data=null, $return=false)
    {
        $loadedData = array();
        $loadedData['header'] = $this->CI->load->view('layout_header','',true);
        $loadedData['sidebar'] = $this->CI->load->view('layout_sidebar','',true);
        $loadedData['content'] = $this->CI->load->view($view,$data,true);
        $loadedData['footer'] = $this->CI->load->view('layout_header','',true);
                
        if($return)
        {
            $output = $this->CI->load->view($this->layout, $loadedData, true);
            return $output;
        }
        else
        {
            $this->CI->load->view($this->layout, $loadedData, false);
        }
    }
}

/* End of file Layout.php */
/* Location: ./application/libraries/Layout.php */