	<div class="page-sidebar-wrapper">
		<div class="page-sidebar navbar-collapse collapse">
			<!-- add "navbar-no-scroll" class to disable the scrolling of the sidebar menu -->
			<!-- BEGIN SIDEBAR MENU -->
			<ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler hidden-phone">
					</div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				</li>
				<li class="sidebar-search-wrapper">
					<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
					<form class="sidebar-search" action="extra_search.html" method="POST">
						<div class="form-container">
							<div class="input-box">
								<a href="javascript:;" class="remove">
								</a>
								<input type="text" placeholder="Search..."/>
								<input type="button" class="submit" value=" "/>
							</div>
						</div>
					</form>
					<!-- END RESPONSIVE QUICK SEARCH FORM -->
				</li>
				<li class="start active">
					<a href="javascript:;">
						<i class="fa fa-shopping-cart"></i>
						<span class="title">
							Transactions
						</span>
						<span class="arrow ">
						</span>
					</a>
					<ul class="sub-menu">
						<li <?php if ($this->uri->segment(1)=="transaction_request"):?>class="active" <?php endif;?>>
							<a href="<?php echo base_url()?>transaction_request">
								<i class="fa fa-bullhorn"></i>
								Purchase Request
							</a>
						</li>
						<li <?php if ($this->uri->segment(1)=="transaction_order"):?>class="active" <?php endif;?>>
							<a href="<?php echo base_url()?>transaction_order">
								<i class="fa fa-shopping-cart"></i>
								Purchase Order
							</a>
						</li>
						<li <?php if ($this->uri->segment(1)=="transaction_invoice"):?>class="active" <?php endif;?>>
							<a href="<?php echo base_url()?>transaction_invoice">
								<i class="fa fa-tags"></i>
								Invoice
							</a>
						</li>
						<li <?php if ($this->uri->segment(1)=="transaction_receive"):?>class="active" <?php endif;?>>
							<a href="<?php echo base_url()?>transaction_receive">
								<i class="fa fa-sitemap"></i>
								Good Receive
							</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="javascript:;">
						<i class="fa fa-cogs"></i>
						<span class="title">
							Master
						</span>
						<span class="arrow ">
						</span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="ecommerce_index.html">
								<i class="fa fa-bullhorn"></i>
								Division
							</a>
						</li>
						<li>
							<a href="ecommerce_orders.html">
								<i class="fa fa-shopping-cart"></i>
								Supplier
							</a>
						</li>
						<li>
							<a href="ecommerce_orders_view.html">
								<i class="fa fa-tags"></i>
								Product
							</a>
						</li>
						<li>
							<a href="ecommerce_products.html">
								<i class="fa fa-sitemap"></i>
								Inventory Location
							</a>
						</li>
					</ul>
				</li>
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>