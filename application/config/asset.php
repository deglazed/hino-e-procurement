<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['asset_path'] 		= 'assets/';
$config['css_path'] 		= 'assets/css/';
$config['download_path'] 	= 'assets/download/';
$config['less_path'] 		= 'assets/less/';
$config['js_path'] 			= 'assets/js/';
$config['img_path'] 		= 'assets/img/';
$config['swf_path'] 		= 'assets/swf/';
$config['upload_path'] 		= 'assets/upload/';
$config['xml_path'] 		= 'assets/xml/';


/* End of file asset.php */