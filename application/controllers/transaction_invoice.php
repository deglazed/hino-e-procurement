<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaction_invoice extends CI_Controller {
	
	public function index()
	{
		$this->layout->view('transaction_invoice');
	}
}

/* End of file transaction_invoice.php */
/* Location: ./application/controllers/transaction_invoice.php */