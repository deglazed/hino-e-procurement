<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaction_Request extends CI_Controller {
	
	public function index()
	{
		$this->layout->view('transaction_request');
	}
}

/* End of file transaction_request.php */
/* Location: ./application/controllers/transaction_request.php */