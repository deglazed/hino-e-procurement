<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaction_Order extends CI_Controller {
	
	public function index()
	{
		$this->layout->view('transaction_order');
	}
}

/* End of file transaction_order.php */
/* Location: ./application/controllers/transaction_order.php */